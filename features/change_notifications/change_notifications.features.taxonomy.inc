<?php
/**
 * @file
 * change_notifications.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function change_notifications_taxonomy_default_vocabularies() {
  return array(
    'properties' => array(
      'name' => 'Properties',
      'machine_name' => 'properties',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
