<?php
/**
 * @file
 * change_notifications.features.inc
 */

/**
 * Implements hook_views_api().
 */
function change_notifications_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function change_notifications_node_info() {
  $items = array(
    'change' => array(
      'name' => t('Change notification'),
      'base' => 'node_content',
      'description' => t('Change notification for Drupal.org, sub-sites, services or infrastructure.'),
      'has_title' => '1',
      'title_label' => t('Short summary'),
      'help' => '',
    ),
    'server' => array(
      'name' => t('Server'),
      'base' => 'node_content',
      'description' => t('A server is a physical or virtual machine that hosts sites and services.'),
      'has_title' => '1',
      'title_label' => t('Server name'),
      'help' => '',
    ),
    'service' => array(
      'name' => t('Service'),
      'base' => 'node_content',
      'description' => t('A service provides functionality or capabilities to sites and is hosted on a server.'),
      'has_title' => '1',
      'title_label' => t('Service name'),
      'help' => '',
    ),
    'site' => array(
      'name' => t('Site'),
      'base' => 'node_content',
      'description' => t('A site is a service with a public-facing URL.'),
      'has_title' => '1',
      'title_label' => t('Site name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
