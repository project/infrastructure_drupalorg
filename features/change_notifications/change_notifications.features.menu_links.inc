<?php
/**
 * @file
 * change_notifications.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function change_notifications_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_change-notifications:changes
  $menu_links['main-menu_change-notifications:changes'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'changes',
    'router_path' => 'changes',
    'link_title' => 'Change notifications',
    'options' => array(
      'identifier' => 'main-menu_change-notifications:changes',
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Change notifications');


  return $menu_links;
}
