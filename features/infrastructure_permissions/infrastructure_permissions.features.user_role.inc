<?php
/**
 * @file
 * infrastructure_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function infrastructure_permissions_user_default_roles() {
  $roles = array();

  // Exported role: admin.
  $roles['admin'] = array(
    'name' => 'admin',
    'weight' => 6,
  );

  // Exported role: community.
  $roles['community'] = array(
    'name' => 'community',
    'weight' => 4,
  );

  // Exported role: confirmed.
  $roles['confirmed'] = array(
    'name' => 'confirmed',
    'weight' => 3,
  );

  // Exported role: email unvalidated.
  $roles['email unvalidated'] = array(
    'name' => 'email unvalidated',
    'weight' => 1,
  );

  // Exported role: infrastructure team.
  $roles['infrastructure team'] = array(
    'name' => 'infrastructure team',
    'weight' => 5,
  );

  return $roles;
}
